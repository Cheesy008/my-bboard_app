from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.template import TemplateDoesNotExist
from django.http import HttpResponse, Http404
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.edit import FormMixin
from django.core.paginator import Paginator
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .models import Category
from .models import Subcategory
from .models import Ad
from .models import Region
from .models import AdditionalImages
from .models import Comment
from .forms import SearchForm
from .forms import AdForm
from .forms import AddImgsFormset
from .forms import GuestCommentForm
from .forms import UserCommentForm
from .filters import AdFilter


def index(request):
    categories = Category.objects.all()
    regions = Region.objects.all()
    ads = Ad.objects.filter(is_active=True)
    if 'keyword' in request.GET:
        keyword = request.GET['keyword']
        q = Q(title__icontains=keyword) | Q(content__icontains=keyword)
        ads = ads.filter(q)
    else:
        keyword = ''
    form = SearchForm(initial={'keyword': keyword})
    paginator = Paginator(ads, 2)
    if 'page' in request.GET:
        page_num = request.GET['page']
    else:
        page_num = 1
    page = paginator.get_page(page_num)
    context = {
        'categories': categories,
        'ads': page.object_list,
        'page': page,
        'regions': regions,
        'form': form,
    }
    return render(request, 'main/index.html', context)


def by_region(request, region):
    categories = Category.objects.all()
    regions = Region.objects.all()
    if region == 'kazahstan':
        ads = Ad.objects.filter(is_active=True)
    else:
        ads = Ad.objects.filter(region__slug=region, is_active=True)
    context = {
        'ads': ads,
        'regions': regions,
        'categories': categories,
    }
    return render(request, 'main/by_region.html', context)


class CategoryDetailView(ListView):
    template_name = 'main/category_detail.html'
    context_object_name = 'subcategories'
    paginate_by = 2

    def get_queryset(self):
        return Subcategory.objects.filter(category__slug=self.kwargs['slug'])

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['current_category'] = Category.objects.get(slug=self.kwargs['slug'])
        context['ads'] = Ad.objects.filter(
            subcategory__category__slug=self.kwargs['slug'],
            is_active=True,
        )
        context['regions'] = Region.objects.all()
        return context


class SubcategoryDetailView(ListView):
    template_name = 'main/subcategory_detail.html'

    def get_queryset(self):
        return Subcategory.objects.filter(slug=self.kwargs['slug'])

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_category'] = Category.objects.get(subcategories__slug=self.kwargs['slug'])
        context['current_subcategory'] = Subcategory.objects.get(slug=self.kwargs['slug'])
        ads = Ad.objects.filter(subcategory__slug=self.kwargs['slug'], is_active=True)
        context['ads'] = ads
        context['filter'] = AdFilter(self.request.GET, queryset=ads)
        return context


def ad_detail(request, slug):
    ad = get_object_or_404(Ad, slug=slug)
    addimgs = AdditionalImages.objects.filter(ad__slug=slug)
    current_category = Category.objects.get(subcategories__ads__slug=slug)
    current_subcategory = Subcategory.objects.get(ads__slug=slug)
    similar_ads = Ad.objects.filter(subcategory=current_subcategory).exclude(slug=slug)
    is_favorite = False
    if ad.favorite.filter(id=request.user.id).exists():
        is_favorite = True

    comments = Comment.objects.filter(ad__slug=slug, is_active=True)
    initial = {'ad': ad}
    if request.user.is_authenticated:
        initial['author'] = request.user.username
        form_class = UserCommentForm
    else:
        form_class = GuestCommentForm
    form = form_class(initial=initial)
    if request.method == 'POST':
        c_form = form_class(request.POST)
        if c_form.is_valid():
            c_form.save(commit=True)
        else:
            form = c_form
    context = {
        'ad': ad,
        'addimgs': addimgs,
        'current_category': current_category,
        'current_subcategory': current_subcategory,
        'similar_ads': similar_ads,
        'comments': comments,
        'is_favorite': is_favorite,
        'form': form,
    }
    return render(request, 'main/ad_detail.html', context)


def additional_page(request, page):
    try:
        template = get_template(f"main/{page}.html")
    except TemplateDoesNotExist:
        raise Http404
    return HttpResponse(template.render(request=request))


@login_required
def ad_create(request):
    if request.method == 'POST':
        form = AdForm(request.POST, request.FILES)
        if form.is_valid():
            ad = form.save()
            formset = AddImgsFormset(request.POST, request.FILES, instance=ad)
            if formset.is_valid():
                formset.save()
                return redirect('main:index')
    else:
        form = AdForm(initial={'author': request.user.id})
        formset = AddImgsFormset()
        context = {'form': form, 'formset': formset}
        return render(request, 'main/ad_create.html', context)


@login_required
def ad_delete(request, slug):
    ad = get_object_or_404(Ad, slug=slug)
    if request.method == 'POST':
        if ad.author == request.user:
            ad.delete()
        return redirect('main:index')
    else:
        return render(request, 'main/ad_delete.html', {'ad': ad})


@login_required
def favorite_ad(request, slug):
    ad = get_object_or_404(Ad, slug=slug)
    if ad.favorite.filter(id=request.user.id).exists():
        ad.favorite.remove(request.user)
    else:
        ad.favorite.add(request.user)
    return redirect(ad.get_absolute_url())


@login_required
def favorite_ads_list(request):
    favorite_ads = Ad.objects.filter(favorite=request.user.pk)
    context = {'favorite_ads': favorite_ads}
    return render(request, 'main/favorite_ads.html', context)


def ad_update(request, slug):
    ad = get_object_or_404(Ad, slug=slug)
    if request.method == 'POST':
        form = AdForm(request.POST, request.FILES, instance=ad)
        if form.is_valid():
            ad = form.save()
            formset = AddImgsFormset(request.POST, request.FILES, instance=ad)
            if formset.is_valid():
                formset.save()
                return redirect('main:index')
    else:
        form = AdForm(instance=ad)
        formset = AddImgsFormset(instance=ad)
        context = {'form': form, 'formset': formset}
        return render(request, 'main/ad_create.html', context)


