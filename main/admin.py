from django.contrib import admin
from django.utils.html import escape, mark_safe
from django.urls import reverse

from .models import Category
from .models import Subcategory
from .models import AdditionalImages
from .models import Region
from .models import Ad
from .models import Comment


class SubcategoryInline(admin.TabularInline):
    model = Subcategory
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    inlines = [SubcategoryInline]


@admin.register(Subcategory)
class SubcategoryAdmin(admin.ModelAdmin):
    def link_to_category(self, obj):
        link = reverse('admin:main_category_change', args=[obj.category.id])
        return mark_safe(f'<a href="{link}">{escape(obj.category.__str__())}</a>')

    link_to_category.short_description = 'Категория'

    list_display = ('name', 'link_to_category',)


class AdditionalImagesInline(admin.TabularInline):
    model = AdditionalImages


@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    def link_to_category(self, obj):
        link = reverse('admin:main_category_change', args=[obj.subcategory.category.id])
        return mark_safe(f'<a href="{link}">{escape(obj.subcategory.category.__str__())}</a>')

    def link_to_subcategory(self, obj):
        link = reverse('admin:main_subcategory_change', args=[obj.subcategory.id])
        return mark_safe(f'<a href="{link}">{escape(obj.subcategory.__str__())}</a>')

    link_to_subcategory.short_description = 'Подкатегория'
    link_to_category.short_description = 'Категория'

    inlines = [AdditionalImagesInline]
    list_display = (
        'link_to_category',
        'link_to_subcategory',
        'title',
        'content',
        'author',
        'created_at'
    )
    list_display_links = ('title',)


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name',)
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Comment)
