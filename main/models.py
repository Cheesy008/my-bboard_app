from django.db import models
from django.urls import reverse
from django.utils.text import slugify

from users.models import CustomUser
from users.utils import get_timestamp_path


class Category(models.Model):
    name = models.CharField(
        max_length=70,
        unique=True,
        db_index=True,
        verbose_name='Название',
    )
    slug = models.SlugField(
        unique=True,
        max_length=80,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('main:category_detail', kwargs={'slug': self.slug})

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Subcategory(models.Model):
    name = models.CharField(
        max_length=70,
        unique=True,
        db_index=True,
        verbose_name='Название',
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.PROTECT,
        related_name='subcategories',
        verbose_name='Категория'
    )
    slug = models.SlugField(
        unique=True,
        max_length=80,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('main:subcategory_detail', kwargs={'slug': self.slug})

    class Meta:
        ordering = ('name',)
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'


class Ad(models.Model):
    slug = models.SlugField(
        unique=True,
        max_length=80,
        blank=True,
    )
    subcategory = models.ForeignKey(
        Subcategory,
        on_delete=models.PROTECT,
        related_name='ads',
        verbose_name='Подкатегория',
    )
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='ads',
        verbose_name='Автор',
    )
    title = models.CharField(
        max_length=70,
        verbose_name='Заголовок объявления'
    )
    content = models.TextField(
        verbose_name='Текст объявления',
    )
    image = models.ImageField(
        upload_to=get_timestamp_path,
        blank=True,
        null=True,
        verbose_name='Изображение',
    )
    price = models.FloatField(
        default=0,
        verbose_name='Цена',
    )
    region = models.ForeignKey(
        'Region',
        on_delete=models.PROTECT,
        related_name='ads',
        verbose_name='Регион',
    )
    contact_details = models.TextField(
        verbose_name='Контакты'
    )
    is_active = models.BooleanField(
        default=True,
        db_index=True,
        verbose_name='Показывать объявление',
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        verbose_name='Опубликовано в'
    )
    favorite = models.ManyToManyField(
        CustomUser,
        related_name='favorites',
        blank=True,
    )

    def get_absolute_url(self):
        return reverse('main:ad_detail', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('main:ad_delete', kwargs={'slug': self.slug})

    def get_edit_url(self):
        return reverse('main:ad_update', kwargs={'slug': self.slug})

    def __str__(self):
        return self.title

    def delete(self, *args, **kwargs):
        for ai in self.addimgs.all():
            ai.delete()
        super().delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        super(Ad, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Объявление'
        verbose_name_plural = 'Объявления'


class Region(models.Model):
    name = models.CharField(
        max_length=50,
        unique=True,
        db_index=True,
        verbose_name='Регион',
    )
    slug = models.SlugField(
        unique=True,
        max_length=80,
        blank=True,
        null=True,
    )

    def get_absolute_url(self):
        return reverse('main:by_region', kwargs={'region': self.slug})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'


class AdditionalImages(models.Model):
    ad = models.ForeignKey(
        Ad,
        on_delete=models.CASCADE,
        related_name='addimgs',
    )
    image = models.ImageField(
        upload_to=get_timestamp_path,
        blank=True,
        null=True,
        verbose_name='Изображение'
    )

    class Meta:
        verbose_name = 'Дополнительное изображение'
        verbose_name_plural = 'Дополнительные изображения'


class Comment(models.Model):
    ad = models.ForeignKey(
        Ad,
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Объявление',
    )
    author = models.CharField(
        max_length=50,
        verbose_name='Автор',
    )
    is_active = models.BooleanField(
        default=True,
        db_index=True,
        verbose_name='Показывать',
    )
    content = models.TextField(verbose_name='Текст')
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        db_index=True,
    )

    def __str__(self):
        return self.ad.title

    class Meta:
        verbose_name_plural = 'Комментарии'
        verbose_name = 'Комментарий'
        ordering = ['-created_at']


