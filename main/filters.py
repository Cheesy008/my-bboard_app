import django_filters

from .models import Ad


class AdFilter(django_filters.FilterSet):
    CHOICES = (
        ('ascending', 'Старые'),
        ('descending', 'Актуальные'),
    )

    price__gt = django_filters.NumberFilter(
        field_name='price',
        lookup_expr='gt',
        label='Цена от',
    )
    price__lt = django_filters.NumberFilter(
        field_name='price',
        lookup_expr='lt',
        label='до',
    )
    ordering = django_filters.ChoiceFilter(
        label='Сортировка по дате',
        choices=CHOICES,
        method='filter_by_ordering',
    )

    def filter_by_ordering(self, queryset, name, value):
        expression = 'created_at' if value == 'ascending' else '-created_at'
        return queryset.order_by(expression)

    class Meta:
        model = Ad
        fields = ['region']



