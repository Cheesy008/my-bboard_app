# Generated by Django 3.0.2 on 2020-01-12 11:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20200112_1756'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='subcategory',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='ads', to='main.Subcategory', verbose_name='Категория'),
        ),
    ]
