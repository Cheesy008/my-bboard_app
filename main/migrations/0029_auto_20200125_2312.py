# Generated by Django 3.0.2 on 2020-01-25 17:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0028_auto_20200125_2311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='subcategory',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='ads', to='main.Subcategory', verbose_name='Подкатегория'),
        ),
    ]
