from django import forms
from django.forms import inlineformset_factory
from captcha.fields import CaptchaField

from .models import Ad
from .models import AdditionalImages
from .models import Comment


class SearchForm(forms.Form):
    keyword = forms.CharField(
        required=False,
        max_length=20,
        label='',
    )


class AdForm(forms.ModelForm):
    class Meta:
        model = Ad
        fields = '__all__'
        labels = {
            'subcategory': 'Подкатегория',
        }
        widgets = {
            'author': forms.HiddenInput,
            'is_active': forms.HiddenInput,
            'slug': forms.HiddenInput,
            'favorite': forms.HiddenInput,
        }


AddImgsFormset = inlineformset_factory(Ad, AdditionalImages, fields='__all__')


class UserCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ('is_active',)
        widgets = {'ad': forms.HiddenInput}


class GuestCommentForm(forms.ModelForm):
    captcha = CaptchaField(
        label='Введите текст с картинки',
        error_messages={'invalid': 'Неверный текст'},
    )

    class Meta:
        model = Comment
        exclude = ('is_active',)
        widgets = {'ad': forms.HiddenInput}



