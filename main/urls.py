from django.urls import path

from .views import CategoryDetailView
from .views import SubcategoryDetailView
from .views import index
from .views import additional_page
from .views import by_region
from .views import ad_detail
from .views import ad_create
from .views import ad_delete
from .views import ad_update
from .views import favorite_ad
from .views import favorite_ads_list

app_name = 'main'

urlpatterns = [
    path('<str:slug>/update/', ad_update, name='ad_update'),
    path('<str:slug>/detail/', ad_detail, name='ad_detail'),
    path('<str:slug>/delete/', ad_delete, name='ad_delete'),
    path('create/', ad_create, name='ad_create'),
    path('favorite_ads_list/', favorite_ads_list, name='favorite_ads_list'),
    path('<str:slug>/favorite_ad/', favorite_ad, name='favorite_ad'),
    path('<str:slug>/subcategory/', SubcategoryDetailView.as_view(), name='subcategory_detail'),
    path('<str:slug>/category/', CategoryDetailView.as_view(), name='category_detail'),
    path('<str:region>/', by_region, name='by_region'),
    path('<str:page>/', additional_page, name='add_page'),
    path('', index, name='index'),
]