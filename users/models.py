from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from django.urls import reverse
from django.dispatch import Signal
from django.dispatch import receiver

from .utils import get_timestamp_path
from .utils import send_activation_notification


user_registrated = Signal(providing_args=['instance'])


@receiver(user_registrated)
def user_registrated_dispatcher(sender, **kwargs):
    send_activation_notification(kwargs['instance'])


class CustomUser(AbstractUser):
    """
    Переопределение стандартной модели пользователя
    """
    username = models.CharField(
        max_length=30,
        verbose_name='Ник',
        unique=True,
    )
    is_activated = models.BooleanField(
        default=True,
        db_index=True,
        verbose_name='Прошёл активацию',
    )
    send_comment_notices = models.BooleanField(
        default=True,
        verbose_name='Уведомления о новых комментариях',
    )
    city = models.ForeignKey(
        'UserCity',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name='Город',
    )
    phone_number = PhoneNumberField(
        blank=True,
        null=True,
        unique=True,
        verbose_name='Номер телефона',
    )
    address = models.CharField(
        verbose_name='Адрес',
        max_length=70,
        blank=True,
        null=True,
    )
    avatar = models.ImageField(
        blank=True,
        null=True,
        upload_to=get_timestamp_path,
        verbose_name='Аватар',
    )

    def get_absolute_url(self):
        return reverse('users:user_profile_ads', kwargs={'pk': self.pk})

    def delete(self, *args, **kwargs):
        for ad in self.ads.all():
            ad.delete()
        super().delete(*args, **kwargs)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class UserCity(models.Model):
    city = models.CharField(
        blank=True,
        null=True,
        max_length=70,
    )

    def __str__(self):
        return self.city

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'







