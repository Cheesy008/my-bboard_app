from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import CustomUser
from .models import UserCity


class MyAdmin(admin.ModelAdmin):
    list_display = [
        'username',
        'first_name',
        'city',
        'email',
        'avatar',
    ]


admin.site.register(CustomUser, MyAdmin)
admin.site.register(UserCity)
