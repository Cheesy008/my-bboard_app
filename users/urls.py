from django.urls import path

from .views import UserLoginView
from .views import UserLogoutView
from .views import UserProfileEditView
from .views import UserProfileView
from .views import DeleteUserView
from .views import UserPasswordChangeView
from .views import RegisterUserView
from .views import RegisterDoneView
from .views import user_activate
from .views import user_profile_ads


app_name = 'users'

urlpatterns = [
    path('accounts/register/activate/<str:sign>/', user_activate, name='register_activate'),
    path('accounts/register/done/', RegisterDoneView.as_view(), name='register_done'),
    path('accounts/register/', RegisterUserView.as_view(), name='register_user'),
    path('accounts/password/change/', UserPasswordChangeView.as_view(), name='password_change'),
    path('accounts/profile/', UserProfileView.as_view(), name='profile'),
    path('accounts/profile/ads/', user_profile_ads, name='user_profile_ads'),
    path('accounts/profile/delete/', DeleteUserView.as_view(), name='delete_user'),
    path('accounts/profile/edit/', UserProfileEditView.as_view(), name='profile_edit'),
    path('accounts/logout/', UserLogoutView.as_view(), name='user_logout'),
    path('accounts/login/', UserLoginView.as_view(), name='user_login'),
]