from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError
from phonenumber_field.formfields import PhoneNumberField
from captcha.fields import CaptchaField
from django.utils.safestring import mark_safe
from string import Template
from django.conf import settings

from .models import CustomUser
from .models import user_registrated


class UserLoginForm(AuthenticationForm):
    username = forms.CharField(
        label='Ваш ник',
        widget=forms.TextInput,
    )
    password = forms.CharField(
        label='Ваш пароль',
        widget=forms.PasswordInput,
    )

    class Meta:
        fields = ['username', 'password']


class UserProfileForm(forms.ModelForm):
    email = forms.EmailField(
        required=True,
        label='Адрес электронной почты',
    )
    phone_number = PhoneNumberField(
        required=True,
        label='Мобильный телефон',
    )

    class Meta:
        model = CustomUser
        fields = [
            'first_name',
            'email',
            'city',
            'phone_number',
            'address',
            'avatar',
            'send_comment_notices',
        ]


class RegisterUserForm(forms.ModelForm):
    email = forms.EmailField(
        required=True,
        label='Адрес электронной почты',
    )
    password1 = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label='Повторите пароль',
        widget=forms.PasswordInput,
    )
    captcha = CaptchaField()

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        if password1:
            password_validation.validate_password(password1)
        return password1

    def clean(self):
        super().clean()
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 and password2 and password1 != password2:
            errors = {
                'password2': ValidationError(
                    'Пароли не совпадают',
                    code='password_mismatch',
                )
            }
            raise ValidationError(errors)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.is_active = False
        user.is_activated = False
        if commit:
            user.save()
        user_registrated.send(RegisterUserForm, instance=user)
        return user

    class Meta:
        model = CustomUser
        fields = [
            'email',
            'username',
            'first_name',
            'password1',
            'password2',
            'captcha',
        ]





