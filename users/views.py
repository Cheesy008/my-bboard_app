from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.core.signing import BadSignature
from django.contrib import messages

from .forms import UserLoginForm
from .forms import UserProfileForm
from .forms import RegisterUserForm
from .models import CustomUser
from .utils import signer
from main.models import Ad


class UserLoginView(LoginView):
    template_name = 'users/registration/login.html'
    authentication_form = UserLoginForm


class UserLogoutView(LoginRequiredMixin, LogoutView):
    next_page = reverse_lazy('main:index')


class UserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'users/profile.html'


@login_required
def user_profile_ads(request):
    ads = Ad.objects.filter(author=request.user.pk)
    context = {'ads': ads}
    return render(request, 'users/profile_ads.html', context)


class UserProfileEditView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = CustomUser
    form_class = UserProfileForm
    template_name = 'users/profile_edit.html'
    success_url = reverse_lazy('users:profile')
    success_message = 'Профиль отредактирован'

    def dispatch(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)


class UserPasswordChangeView(SuccessMessageMixin, LoginRequiredMixin, PasswordChangeView):
    template_name = 'users/password_change.html'
    success_url = reverse_lazy('users:profile')
    success_message = 'Ваш пароль изменён'


class RegisterUserView(CreateView):
    model = CustomUser
    form_class = RegisterUserForm
    template_name = 'users/registration/register_user.html'
    success_url = reverse_lazy('users:register_done')


class RegisterDoneView(TemplateView):
    template_name = 'users/registration/register_done.html'


def user_activate(request, sign):
    try:
        username = signer.unsign(sign)
    except BadSignature:
        return render(request, 'users/registration/bad_signature.html')
    user = get_object_or_404(CustomUser, username=username)
    if user.is_activated:
        template = 'users/user_is_activated.html'
    else:
        template = 'users/activation_done.html'
        user.is_activated = True
        user.is_active = True
        user.save()
    return render(request, template)


class DeleteUserView(LoginRequiredMixin, DeleteView):
    model = CustomUser
    template_name = 'users/delete_user.html'
    success_url = reverse_lazy('main:index')

    def dispatch(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)

    def post(self, request, *args, **kwargs):
        logout(request)
        messages.add_message(
            request,
            messages.SUCCESS,
            'Аккаунт удалён',
        )
        return super().post(request, *args, **kwargs)






