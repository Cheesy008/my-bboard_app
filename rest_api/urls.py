from django.urls import path

from .views import ad
from .views import AdDetailView

app_name = 'api'

urlpatterns = [
    path('ads/<int:pk>/', AdDetailView.as_view()),
    path('ads/', ad),
]