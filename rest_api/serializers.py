from rest_framework import serializers

from main.models import Ad


class AdSerializer(serializers.ModelSerializer):
    region_name = serializers.CharField(source='region.name')

    class Meta:
        model = Ad
        fields = (
            'id',
            'slug',
            'title',
            'region_name',
            'content',
            'created_at',
            'author',
        )


class AdDetailSerializer(serializers.ModelSerializer):
    region_name = serializers.CharField(source='region.name')

    class Meta:
        model = Ad
        fields = (
            'id',
            'title',
            'region_name',
            'content',
            'image',
            'author',
            'contact_details',
        )
