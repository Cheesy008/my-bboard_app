from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.generics import RetrieveAPIView

from .serializers import AdSerializer
from .serializers import AdDetailSerializer
from main.models import Ad


@api_view(['GET'])
def ad(request):
    if request.method == 'GET':
        ads = Ad.objects.filter(is_active=True)[:10]
        serializer = AdSerializer(ads, many=True)
        return Response(serializer.data)


class AdDetailView(RetrieveAPIView):
    queryset = Ad.objects.filter(is_active=True)
    serializer_class = AdDetailSerializer



